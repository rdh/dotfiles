# Path to your oh-my-zsh installation.
export ZSH=${HOME}/.oh-my-zsh

ZSH_THEME="robbyrussell"

plugins=(docker git git-prompt gradle rbenv systemd cargo rust)

# User configuration

export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"

# Local bin path
export PATH=$PATH:$HOME/bin

source $ZSH/oh-my-zsh.sh

# Anaconda setup

if [ -d "$HOME/anaconda3" ]
then
    export PATH="$HOME/anaconda3/bin:$PATH"
fi

# Cargo setup
export PATH="$HOME/.cargo/bin:$PATH"

# OSX/Linux specific stuff
os=`uname`
if [ "$os" = "Darwin" ]
then
    # Set JAVA_HOME
	export JAVA_HOME=`/usr/libexec/java_home`

    # Alias MacVim if installed
    if [ -e `which mvim` ]
    then
        alias vim="mvim -v"
        export EDITOR="mvim -v"
    fi
else
    # Set JAVA_HOME
	JAVAC=/usr/bin/javac
	if [ -e $JAVAC ]
	then
	    export JAVA_HOME=`$READLINK -f $JAVAC | xargs dirname | xargs dirname`
	fi
fi

# Aliases
alias b='gradle --daemon'
alias bb='b build'
