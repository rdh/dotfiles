#!/bin/sh

device=`xinput | grep Touchpad | awk -F= '{print $2}' | awk -F' ' '{print $1}'`
enabled=$(xinput list-props $device | grep "Device Enabled" | cut -d: -f2 | sed 's/\s//g')

if [ "$enabled" -eq "1" ];
then
    flag="0"
else
    flag="1"
fi

xinput set-prop $device "Device Enabled" $flag
